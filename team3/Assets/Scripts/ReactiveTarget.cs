﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReactiveTarget : MonoBehaviour {
    public GameObject wall;

    private string healthString;
    private int healthnum;
    
    public void Reacttohit()
    {
        numAdjust(healthnum, 10);
        
    }
    private IEnumerator Die()
    {
        Instantiate(wall, this.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.0f);
        Destroy(this.gameObject);
        
    }
    // Use this for initialization
    void Start()
    {
        healthnum = 100;
        healthString = " ";

    }

    // Update is called once per frame
    void Update()
    {
        Object body = GameObject.FindWithTag("Finish");
        Destroy(body, 10);


    }
    void numAdjust(int health, int numDecrease)
    {
        if (health > 0)
        {
          
            healthString = "Health ：" + healthnum.ToString ();
            healthnum -= numDecrease;

        }
        else
        {
            healthString = "die";
            StartCoroutine(Die());
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(600, 200, 100, 20), healthString);
       
        GUI.skin.label.normal.textColor = new Vector4(255, 0, 0, 1);
    }

}
